import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { environment } from '../../environments/environment';
/*
  Generated class for the OpenWeatherMapProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class OpenWeatherMapProvider {

  apiUrl = environment.weatherMapApi.baseUrl;
  apiKey = environment.weatherMapApi.key;
  apiLang = environment.lang;
  apiUnit = environment.weatherMapApi.units;

  constructor(public http: HttpClient) {
    console.log('Hello OpenWeatherMapProvider Provider');
  }

  getWeatherForCoords(latitude, longitude){
    let params = new HttpParams()
      .set("lat", latitude)
      .set("lon", longitude)
      .set("lang", this.apiLang)
      .set("units", this.apiUnit)
      .set("APPID", this.apiKey);

    return new Promise(resolve => {
      this.http.get(this.apiUrl+'/weather', {params: params}).subscribe(data => {
        console.log("data : ", data);
        resolve(data);
      }, err => {
        console.log(err);
      });
    });
  }

  getWeatherForCityName(cityName){
    let params = new HttpParams()
      .set("q", cityName)
      .set("lang", this.apiLang)
      .set("units", this.apiUnit)
      .set("APPID", this.apiKey);

    return new Promise(resolve => {
      this.http.get(this.apiUrl+'/weather', {params: params}).subscribe(data => {
        console.log("data : ", data);
        resolve(data);
      }, err => {
        console.log(err);
      });
    });
  }

  getForecastForCoords(latitude, longitude){
    let params = new HttpParams()
      .set("lat", latitude)
      .set("lon", longitude)
      .set("lang", this.apiLang)
      .set("units", this.apiUnit)
      .set("APPID", this.apiKey);

    return new Promise(resolve => {
      this.http.get(this.apiUrl+'/forecast', {params: params}).subscribe(data => {
        console.log("data : ", data);
        resolve(data);
      }, err => {
        console.log(err);
      });
    });
  }

}
