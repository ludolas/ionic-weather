import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';

import { OpenWeatherMapProvider } from '../../providers/open-weather-map/open-weather-map';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  currentLocation: Coordinates;

  currentCityName: any;
  weatherData: any;
  forecastData: any;

  constructor(public navCtrl: NavController, private geolocation: Geolocation, private openWeatherMapProvider : OpenWeatherMapProvider) {
    this.getCurrentLocation();
  }

  getCurrentLocation(){
    this.geolocation.getCurrentPosition().then((resp) => {
      console.log("you are here ", resp.coords.latitude, resp.coords.longitude, this);
      this.currentLocation = resp.coords;
      this.getCurrentWeather();
      this.getForecast();
     }).catch((error) => {
       console.log('Error getting location', error);
     });
     
  }

  getCurrentWeather(){
    this.openWeatherMapProvider.getWeatherForCoords(this.currentLocation.latitude, this.currentLocation.longitude).then((data) => {
      this.weatherData = data;
      this.currentCityName = data["name"];
    });
  }
  
  getForecast(){
    this.openWeatherMapProvider.getForecastForCoords(this.currentLocation.latitude, this.currentLocation.longitude).then((data) => {
      console.log(data);
      this.forecastData = data["list"];
    });
  }

}
