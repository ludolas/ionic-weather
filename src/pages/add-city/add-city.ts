import { Component } from '@angular/core';
import { IonicPage,  NavParams, ViewController } from 'ionic-angular';
import { 
  FormBuilder,
  FormGroup,
  Validators
 } from '@angular/forms';
 import { NativeGeocoder, NativeGeocoderForwardResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder';
 import { Storage } from '@ionic/storage';


/**
 * Generated class for the AddCityPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-city',
  templateUrl: 'add-city.html',
})
export class AddCityPage {

  public form : FormGroup;

  constructor(private navParams: NavParams, private view: ViewController, private formBuilder: FormBuilder, private nativeGeocoder: NativeGeocoder, private storage: Storage) {
    this.form = formBuilder.group({
      'cityName'        : ['', Validators.required]
   });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddCityPage');
  }

  closeModal(){
    this.view.dismiss();
  }

  addCity(formValue){
    console.log(formValue);
    console.log(formValue.cityName);


    this.storage.length().then((l) => {
      this.storage.set(l.toString(), formValue.cityName).then(() => {
        const data = {
          cityName : formValue.cityName
        }; 
        this.view.dismiss(data);
      });
    })
    
    // try to geoCode it, if ok, add it to storage !
    // only for ios & android :(
        // let options: NativeGeocoderOptions = {
        //     useLocale: true,
        //     maxResults: 5
        // };
    // this.nativeGeocoder.forwardGeocode(formValue.cityName, options)
    //   .then((coordinates: NativeGeocoderForwardResult[]) => console.log('The coordinates are latitude=' + coordinates[0].latitude + ' and longitude=' + coordinates[0].longitude))
    //   .catch((error: any) => console.log(error));
  }

}
