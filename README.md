# ionic-weather

## Pre-requis
Node.js : https://nodejs.org/en/ 

## Packages NPM global
```sh
 npm i -g cordova
```

## Package Cordova 
```sh
ionic cordova plugin add cordova-sqlite-storage
ionic cordova plugin add cordova-plugin-nativegeocoder
ionic cordova plugin add cordova-plugin-geolocation --variable GEOLOCATION_USAGE_DESCRIPTION="To locate you"
```

## Application
```sh
git clone https://framagit.org/ludolas/ionic-weather.git
cd ionic-weather
npm install
ionic serve
```
