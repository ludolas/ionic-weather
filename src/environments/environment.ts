export const environment = {
    weatherMapApi : {
        key : "5f8e48885cb33b2286c595f5be800276",
        baseUrl : "https://api.openweathermap.org/data/2.5/",
        units : "metric"
    },
    lang : "fr"
};