import { Component } from '@angular/core';
import { ModalController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { OpenWeatherMapProvider } from '../../providers/open-weather-map/open-weather-map';

@Component({
  selector: 'page-favorites',
  templateUrl: 'favorites.html'
})
export class FavoritesPage {
  favorites: any;

  constructor(private modal: ModalController, private storage: Storage, private weatherProv: OpenWeatherMapProvider) {
    this.getFavorites();
  }

  getFavorites(){
    this.favorites = [];
    this.storage.ready().then(() => {
      this.storage.keys().then(keys => {
        console.log(keys);
        keys.forEach(key => {
          this.storage.get(key).then(cityName => {
            this.weatherProv.getWeatherForCityName(cityName).then(data => {
              this.favorites.push(data);
            });
          });
        })
      })
    })
  }

  addCity(){
    console.log("add city");
    const addCityModal = this.modal.create('AddCityPage');
    addCityModal.onDidDismiss(data => {
      if (!data){
        return;
      }
      this.weatherProv.getWeatherForCityName(data.cityName).then(data => {
        console.log(this.favorites);
        this.favorites.push(data);
        console.log(this.favorites);
      });
    });
    addCityModal.present();
  }

}
